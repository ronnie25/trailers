<?php

namespace App\Controller;

use App\Repository\SearchRepo;
use App\Repository\TrailerRepo;
use Slim\Http\Request;
use Slim\Http\Response;
use Tmdb\Model\Movie;
use Tmdb\Model\Search\SearchQuery\MovieSearchQuery;

class IndexController extends AbstractController
{
    /**
     * Home page
     *
     * @param Request $request
     * @param Response $response
     * @return \Psr\Http\Message\ResponseInterface|Response
     */
    public function indexAction(Request $request, Response $response)
    {
        $response = $this->view->render($response, 'index.phtml');
        return $response;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws \Exception
     */
    public function searchAction(Request $request, Response $response)
    {
        $search = mb_strtolower(trim(strip_tags($request->getParam('q'))));

        if (!$search || strlen($search) < 3) {
            throw new \Exception('At least 3 symbols required for search');
        }

        // Check if query is cached
        $cache = SearchRepo::fetchSearchQuery($search);
        if ($cache) {
            $response = $response->withJson(json_decode($cache['result'], true));
            return $response;
        }

        /** @var  \Tmdb\Repository\SearchRepository $repository */
        $repository = $this->__get('movieSearch');
        $query = new MovieSearchQuery();
        $query->page(1);
        $find = $repository->searchMovie($search, $query);

        $data = [];
        foreach ($find as $movie) {
            // Skip adult movies
            if ($movie->getAdult()) {
                continue;
            }

            if (!$movie->getPosterPath()) {
                continue;
            }

            /** @var Movie $movie */
            $data[] = [
                'name' => $movie->getTitle(),
                'desc' => $movie->getOverview(),
                'poster' => $movie->getPosterImage()->getFilePath(),
                'backdrop' => $movie->getBackdropImage()->getFilePath(),
                'releaseDate' => $movie->getReleaseDate()->format('Y-m-d'),
                'trailer' => null,
            ];
        }

        // Cache result
        SearchRepo::delete(['query' => $search]);
        SearchRepo::create(['query' => $search, 'result' => json_encode($data), 'updated' => (new \DateTime())->format('Y-m-d H:i:s')]);

        $response = $response->withJson($data);
        return $response;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws \Exception
     */
    public function findTrailersAction(Request $request, Response $response)
    {
        $search = $request->getParam('search');

        if (empty($search)) {
            throw new \Exception('Param search is required');
        }

        if (is_string($search)) {
            $search = [$search];
        }

        foreach ($search as $index => $value) {
            $search[$index] = trim(strip_tags($value));
        }

        $data = [];
        $result = TrailerRepo::fetchTrailersByMovieNames($search);
        foreach ($result as $row) {
            $data[$row['name']] = $row['trailer_code'];
        }

        $youtube = $this->__get('youtubeService');
        foreach ($search as $movieName) {
            if (isset($data[$movieName])) {
                continue;
            }

            $result = $youtube->search->listSearch('snippet', ['q' => $movieName . ' movie trailer', 'type' => 'video']);
            foreach ($result as $item) {
                /** @var \Google_Service_YouTube_SearchResult $item */
                $videoId = $item->getId()->getVideoId();
                $data[$movieName] = $videoId;
                TrailerRepo::create(['name' => $movieName, 'trailer_code' => $videoId]);
                continue 2;
            }

            throw new \Exception('No results for ' . $movieName);
        }


        $response = $response->withJson($data);
        return $response;
    }
}