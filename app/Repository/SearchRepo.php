<?php


namespace App\Repository;


use App\Util\Db;

class SearchRepo extends AbstractRepo
{
    const TABLE = 'searches';

    public static function fetchSearchQuery($query)
    {
        $db = Db::getDb();
        $threeDaysAgo = (new \DateTime())->modify('-3 days');
        $result = $db->get(self::TABLE, ['query', 'result'], ['query' => $query, 'updated[>=]' => $threeDaysAgo->format('Y-m-d H:i:s')]);
        return $result;
    }
}