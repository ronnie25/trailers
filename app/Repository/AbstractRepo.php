<?php


namespace App\Repository;


use App\Util\Db;

abstract class AbstractRepo
{
    const TABLE ='table_name';

    /**
     * Inserts a row in the selected table
     * Returns the id of the inserted row
     *
     * @param $data
     * @return int
     */
    public static function create(array $data): int
    {
        $db = Db::getDb();
        $db->insert(static::TABLE, $data);
        return $db->id();
    }

    /**
     * Updates a row by a provided id in the selected table
     *
     * @param array $data
     * @return bool
     */
    public static function update(array $data)
    {
        $db = Db::getDb();
        $id = $data['id'];
        unset($data['id']);

        $db->update(static::TABLE, $data, ['id' => $id]);
        return true;
    }

    /**
     * @param array $where
     * @return true
     */
    public static function delete(array $where)
    {
        $db = Db::getDb();
        $db->delete(static::TABLE, $where);
        return true;
    }

}