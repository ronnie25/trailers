<?php


namespace App\Repository;


use App\Util\Db;

class TrailerRepo extends AbstractRepo
{
    const TABLE = 'trailers';

    public static function fetchTrailersByMovieNames($movies)
    {
        $db = Db::getDb();
        $result = $db->select(self::TABLE, ['name', 'trailer_code'], ['name' => $movies]);

        return $result;
    }
}