<?php

// Cli Routes
/** @var Slim\App $app */
$app->get('/{command}/{method}', \App\Controller\CliController::class . ':process');
