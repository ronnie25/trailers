<?php
// DIC configuration

/** @var Slim\App $app */

$container = $app->getContainer();

// View Object
$container['view'] = function ($c) {
    $phpView = new \Slim\Views\PhpRenderer(APP_PATH . '/view/');
    $phpView->setLayout('layout.phtml');
    /** @var \Slim\Http\Request $request */
    $request = $c->get('request');
    /** @var \Slim\Http\Uri $uri */
    $uri = $request->getUri();
    $phpView->addAttribute('baseUrl', $uri->getBaseUrl());
    $phpView->addAttribute('basePath', $uri->getBasePath());
    $phpView->addAttribute('path', $uri->getBasePath() . $uri->getPath());

    return $phpView;
};

$container['db'] = function ($c) {
    /** @var \Psr\Container\ContainerInterface $c */
    $settings = $c->get('settings')['db'];
    $db = new \Medoo\Medoo($settings);
    unset($settings);
    return $db;
};
\App\Util\Db::setDb($container->get('db'));

$container['movieSearch'] = function ($c) {
    $apiToken = $settings = $c->get('settings')['movie_service']['api_token'];
    $token  = new \Tmdb\ApiToken($apiToken);
    $client = new \Tmdb\Client($token);
    $repository = new \Tmdb\Repository\SearchRepository($client);
    return $repository;
};

$container['youtubeService'] = function ($c) {
    $client = new \Google_Client();
    $apiKey = $settings = $c->get('settings')['google_service']['api_key'];
    $client->setDeveloperKey($apiKey);
    $youtube = new \Google_Service_YouTube($client);
    return $youtube;
};