<?php

use App\Controller\DashboardController;
use App\Controller\IndexController;
use App\Middleware\Auth;
use App\Middleware\EpayAuth;
use App\Middleware\EpayCallback;
use App\Middleware\ExistingSubscription;
use App\Middleware\IpAccessLimit;

// Routes
/** @var Slim\App $app */
$container = $app->getContainer();

$app->get('/', IndexController::class . ':indexAction');
$app->get('/api/search', IndexController::class . ':searchAction');
$app->get('/api/find-trailers', IndexController::class . ':findTrailersAction');
