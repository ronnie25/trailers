<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
set_time_limit(0);
define('APP_PATH', __DIR__ . '/app');
define('ROOT_PATH', __DIR__);

require ROOT_PATH . '/vendor/autoload.php';

// Instantiate the app
$settings = require ROOT_PATH . '/app/config/settings.php';
// Convert $argv to PATH_INFO and mock console environment
$argv = $GLOBALS['argv'];
array_shift($argv);

$params = [];
for ($i = 2; $i < count($argv); ++$i) {
    [$key, $value] = explode("=", $argv[$i], 2);
    $params[$key] = $value;
}

$settings['environment'] = \Slim\Http\Environment::mock([
    'SCRIPT_NAME' => $_SERVER['SCRIPT_NAME'],
    'REQUEST_URI' => count($argv) >= 2 ? "/{$argv[0]}/{$argv[1]}" : "/help",
    'QUERY_STRING' => http_build_query($params),
]);
$app = new \Slim\App($settings);

// Set up dependencies
require ROOT_PATH . '/app/config/dependencies.php';

// Register middleware
require ROOT_PATH . '/app/config/middleware.php';

// Register routes
require ROOT_PATH . '/app/config/cli-routes.php';

// Run app
$app->run();