# trailers

Search for movie trailers

Two APIs are used for this project:
The movie database API
Google API (for youtube videos)

###Install

Clone project

Install composer dependencies

```composer install```

Copy and edit the configuration

```cp app/config/settings.php.dist app/config/settings.php```

Import database.sql

#### Project currently runs on https://trailers.ronnie.xyz